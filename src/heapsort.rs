use std::ops::DerefMut;
use crate::traits::*;

fn parent(i: usize) -> usize {
    (i-1)/2
}
fn left(i: usize) -> usize {
    2*i + 1
}
fn right(i: usize) -> usize {
    2*i + 2
}

fn sift_down<E>(arr: &mut[E], start: usize, end: usize, f: &Fn(&E, &E) -> bool) {
    let mut root = start;
    while left(root) <= end {
        let l = left(root);
        let r = right(root);
        let mut swap = root;
        if f(&arr[swap], &arr[l]) {
            swap = l;
        }
        if r <= end && f(&arr[swap], &arr[r]) {
            swap = r;
        }
        if swap == root {
            return;
        } else {
            arr.swap(root, swap);
            root = swap;
        }
    }
}

fn build_heap<E>(arr: &mut[E], f: &Fn(&E, &E) -> bool) {
    let end = arr.len() - 1;
    let mut start = parent(end);
    for i in (0..start).rev() {
        start = i;
        sift_down(arr, start, end, f);
    }
}

fn do_sort<E>(arr: &mut[E], f: &Fn(&E, &E) -> bool) {
    if  arr.len() > 1 {
        build_heap(arr, f);
        for i in (1..arr.len()).rev() { 
            arr.swap(i, 0);
            let end = i-1;
            sift_down(arr, 0, end, f);
        }
    }
}

fn sort_by<E, F: Fn(&E, &E) -> bool>(arr: &mut[E], f: F) {
    do_sort(arr, &f);
}

impl<E, S: DerefMut<Target=[E]>> HeapsortBy for S {
    type Item = E;
    fn heapsort_by<F: Fn(&Self::Item, &Self::Item) -> bool>(&mut self, f: F) {
        sort_by(self, f);
    }
}

impl<H: HeapsortBy> Heapsort for H where H::Item: PartialOrd {
    fn heapsort(&mut self) {
        self.heapsort_by(|a, b| a < b);
    }
}
