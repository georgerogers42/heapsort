#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate clap;
extern crate regex;

extern crate heapsort;

#[cfg(test)]
mod tests;

use std::io::prelude::*;
use std::io;
use regex::Regex;
use std::error::Error;
use std::fmt;
use heapsort::traits::*;

fn get_lines<R: BufRead>(r: R) -> io::Result<Vec<String>> {
    let mut lines = vec![];
    for line in r.lines() {
        let line = line?;
        lines.push(line);
    }
    Ok(lines)
}

#[derive(Debug)]
pub enum ParseError<E> {
    NoMatch,
    Other(E),
}

impl<E> ParseError<E> {
    pub fn new() -> ParseError<E> {
        ParseError::NoMatch
    }
}

impl<E> fmt::Display for ParseError<E> {
    fn fmt(&self, w: &mut fmt::Formatter) -> fmt::Result {
        write!(w, "No Match")
    }
}

impl<E> From<E> for ParseError<E> {
    fn from(e: E) -> Self {
        ParseError::Other(e)
    }
}

impl<E : Error> Error for ParseError<E> {
    fn description(&self) -> &str {
        match self {
            &ParseError::NoMatch => {
                "no match"
            } &ParseError::Other(ref e) => {
                e.description()
            }
        }
    }
    fn cause(&self) -> Option<&Error> {
        match self {
            &ParseError::NoMatch => {
                None
            } &ParseError::Other(ref e) => {
                Some(e)
            }
        }
    }
}

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
struct Verse {
    book: String,
    chapter: u64,
    verse: u64,
    contents: String,
}

impl fmt::Display for Verse {
    fn fmt(&self, w: &mut fmt::Formatter) -> fmt::Result {
        write!(w, "{} {}:{} {}", self.book, self.chapter, self.verse, self.contents)
    }
}

lazy_static! {
    static ref VERSE: Regex = Regex::new(r"^\s*([a-zA-Z0-9]+)\s+(\d+):(\d+)\s(.+)$").unwrap();
}

fn get_verses<R: BufRead>(r: R) -> Result<Vec<Verse>, ParseError<io::Error>> {
    let mut lines = vec![];
    for line in r.lines() {
        let line = line?;
        let vc = VERSE.captures(&line).ok_or(ParseError::new())?;
        let book = vc[1].to_owned();
        let (chap, verse) = (vc[2].parse().unwrap(), vc[3].parse().unwrap());
        let contents = vc[4].to_owned();
        lines.push(Verse{ book, chapter: chap, verse, contents});
    }
    Ok(lines)
}

fn rev_cverse(a: &Verse, b: &Verse) -> bool {
    (&a.book, b.chapter, b.verse, &a.contents) > (&b.book, a.chapter, a.verse, &b.contents)
}

fn sort_bible<W: Write, R: BufRead>(reverse: bool, w: W, r: R) {
    let mut verses = get_verses(r).unwrap();
    if reverse {
        verses.heapsort_by(rev_cverse);
    } else {
        verses.heapsort();
    }
    report(w, verses.iter().map(|v| v.to_string())).unwrap();
}

fn sort_strings<W: Write, R: BufRead>(reverse: bool, w: W, r: R) {
    let mut lines = get_lines(r).unwrap();
    lines.heapsort();
    if reverse {
        report(w, lines.iter().rev()).unwrap();
    } else {
        report(w, &lines).unwrap();
    }
}

fn report<W, Line, Lines>(mut w: W, lines: Lines) -> io::Result<()> where W: Write, Line: AsRef<str>, Lines: IntoIterator<Item=Line> {
    for line in lines {
        writeln!(w, "{}", line.as_ref())?;
    }
    Ok(())
}

fn main() {
    let ap = app_from_crate!()
        .arg(clap::Arg::with_name("bible").short("b").long("bible"))
        .arg(clap::Arg::with_name("reverse").short("r").long("reverse"))
        .get_matches();
    let stdin = io::stdin();
    let stdout = io::stdout();
    let reverse = ap.is_present("reverse");
    let bible = ap.is_present("bible");
    if bible {
        sort_bible(reverse, stdout.lock(), stdin.lock());
    } else {
        sort_strings(reverse, stdout.lock(), stdin.lock());
    }
}
